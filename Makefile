PREFIX = $(DESTDIR)/usr/sbin
MANDIR = $(DESTDIR)/usr/share/man/man8

all: man

man : 
	pod2man -s 8 -c ' ' ./sysv-rc-conf.pl | gzip -9 -c > sysv-rc-conf.8.gz

install : man
	mkdir -p $(PREFIX) $(MANDIR)
	install -m755 ./sysv-rc-conf.pl $(PREFIX)/sysv-rc-conf
	install -m644 ./sysv-rc-conf.8.gz $(MANDIR)

uninstall :
	rm -f $(PREFIX)/sysv-rc-conf
	rm -f $(MANDIR)/sysv-rc-conf.8.gz

clean :
	rm -f sysv-rc-conf.8.gz
